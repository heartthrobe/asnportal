const fs     = require('fs');
const path   = require('path');
var   cluster = require('cluster');
const { cpus }   = require('os')
const numWorkers = cpus().length
const isMaster   = cluster.isMaster
let   portNo = 5001;
var   app  = require('../app');
var   http = require('http');
var   port = normalizePort(process.env.PORT || portNo);
let   server = http.createServer(app);//create server

app.set('port',port);//app listing on port

if(isMaster){
    const workers = [...Array(numWorkers)].map(_ => cluster.fork())
    cluster.on('online', (worker) => console.log(`Worker ${worker.process.pid} is online`))
    cluster.on('exit', (worker, exitCode) => {
        console.log(`Worker ${worker.process.id} exited with code ${exitCode}`)
        console.log(`Starting a new worker`)
        cluster.fork()
    })
}else{

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);
}

    /**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
    // named pipe
    return val;
    }

    if (port >= 0) {
    // port number
    return port;
    }

    return false;
}

/**
* Event listener for HTTP server "listening" event.
*/

function onListening() {
    var addr = server.address();
    console.log(addr);
    console.log('Process ' + process.pid + ' is listening to all incoming requests');
    var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
// debug('Listening on ' + bind);
}

/**
* Event listener for HTTP server "error" event.
*/
function onError(error) {
    if (error.syscall !== 'listen') {
    console.log("*************************************");
    console.log("Error",error);
    console.log("*************************************");
    console.log("//////////////////////////////////////");
    }

    var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
    case 'EACCES':
        console.error(bind + ' requires elevated privileges');
    // process.exit(1);
        break;
    case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        //   process.exit(1);        
        break;
    default:
        // throw error;
        console.log("*************************************");
        console.log("Error",error);
        console.log("*************************************");
        console.log("//////////////////////////////////////");
    }
}


