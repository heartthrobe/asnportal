var router = express.Router();
passport = require('passport');
bcrypt = require('bcryptjs');
LocalStrategy = require('passport-local').Strategy;
fs = require('fs');
path = require('path');
moment = require('moment');
moment_tz = require('moment-timezone');
var dbUsreInfo = require('../../libs/sqlConnection')

router.get('/getBuyerDetails', (req, res) => {
    try {
        dbUsreInfo.get_Sp_BuyerInfo().then(buyerInfo => {
            
            res.send({
                status: 200,
                msg: "Vendor Added Successfully",
                result: buyerInfo,
                error: undefined
            })
        }).catch(error => {
            res.send({
                status: 400,
                msg: "",
                result: "Something Went Wrong",
                error: error
            })
        })
    } catch (error) {
        reject({
            status: 400,
            msg: "syntax error",
            error: error,
            result: ''
        })
    }
})

module.exports = router;