var express         = require('express');
    router          = express.Router();
    passport        = require('passport');
    bcrypt          = require('bcryptjs');
    LocalStrategy   = require('passport-local').Strategy;
    fs              = require('fs');
    path            = require('path');
    moment          = require('moment');
    moment_tz       = require('moment-timezone');



router.post('/login', (req, res, next) => {
    try {
        console.log(req.body);
    //  console.log("Request Protocol" + req.protocol + " Reequest Headers" + JSON.stringify(req.headers, null, 4));
      passport.authenticate('local', function (err, user, info) {
        if (err) { 
            return next(err);
         }
      
        if (!user) { 
            return res.send({ status : 400 ,
                msg : "user not found" , 
                result : '' , 
                error : undefined });
         }else{
            req.logIn(user, function (err) {
                if (err) { return next(err); }
                return res.send({ status : 200 ,
                     msg : "Successfully user login" , 
                     result : user , 
                     error : undefined });
            });
         }
           
      })(req, res, next);
  
    } catch (error) {
       res.send({ status : 400 ,
            msg : "error" , 
            result : '' , 
            error : error })
    }
  });

router.get('/logout',(req, res) => {
    try {
     
      req.session.destroy(function (err) {
       // console.log(err);
        res.clearCookie('connect.sid');
        req.logOut();
        return res.send({status:200,msg:"successfully logout",result:'',error:undefined});
      });
    } catch (error) {
        return res.send({status:400,msg:"successfully logout",result:'',error:error});
    }
  });

  module.exports = router; 