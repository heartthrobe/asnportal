var router = express.Router();
passport = require('passport');
bcrypt = require('bcryptjs');
LocalStrategy = require('passport-local').Strategy;
fs = require('fs');
path = require('path');
moment = require('moment');
moment_tz = require('moment-timezone');
var dbUsreInfo = require('../../libs/sqlConnection')


router.post('/registerVendor', (req, res) => {
    try {
        console.log(req.body);
        // if(req.authenticate())
        // {

        let vendorDetails = {
            vendorCode: req.body.vendorCode,
            vendorName: req.body.vendorName,
            emailID: req.body.vendorEmail,
            mobileNo: req.body.vendorMobileNo,
            division: req.body.vendorDivision,
            buyerName: req.body.vendorBuyer,
            buyerEmailID: req.body.vendorBuyerEmail,
            

        }
        console.log("User details",vendorDetails)
        dbUsreInfo.inset_Sp_VendorInfo(vendorDetails).then(vendorInfo => {
            console.log("111", vendorInfo)
            res.send({
                status: 200,
                msg: "Vendor Added Successfully",
                result: vendorInfo,
                error: undefined
            })
        }).catch(error => {
            res.send({
                status: 400,
                msg: "",
                result: "Something Went Wrong",
                error: error
            })
        })
    } catch (error) {
        reject({
            status: 400,
            msg: "syntax error",
            error: error,
            result: ''
        })
    }
})

router.get('/getVendorDetailsIdWise', (req, res) => {
    try {
        dbUsreInfo.get_Sp_VendorInfoIdWise().then(vendorInfo => {
            
            res.send({
                status: 200,
                msg: "Vendor Exist",
                result: vendorInfo,
                error: undefined
            })
        }).catch(error => {
            res.send({
                status: 400,
                msg: "",
                result: "Something Went Wrong",
                error: error
            })
        })
    } catch (error) {
        reject({
            status: 400,
            msg: "syntax error",
            error: error,
            result: ''
        })
    }
})

    //         }
    //         else{

    //             res.send({ status : 400 ,
    //                 msg : "Session expired" , 
    //                 result : 'Oops session expired' , 
    //                 error : undefined })
    //         }

    //     } catch (error) {
    //        res.send({ status : 401 ,
    //             msg : "error" , 
    //             result : '' , 
    //             error : error })
    //     }
    //   });

    module.exports = router;