var express         = require('express');
    router          = express.Router();
    fs              = require('fs');



const dbPOInfo = require('../../libs/sp_po_Detail');


router.get('/podetail', (req, res, next) => {
    try {
        console.log("IN podetail");
        dbPOInfo.get_Sp_Po_Detail().then(result=>{
            return res.send(result);
        }).catch(error=>{
            res.send({ status : 400 ,
                msg : "error occure in po_detail api" , 
                result : '' , 
                error : error })
        })
    } catch (error) {
       res.send({ status : 400 ,
            msg : "error" , 
            result : '' , 
            error : error })
    }
  });

module.exports = router; 