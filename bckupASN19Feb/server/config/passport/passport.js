const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
//module import
const dbUserInfo = require('../../libs/sqlConnection');


// // passport needs ability to serialize and unserialize users out of session
passport.serializeUser(function (user, done) {
  console.log(user);
    return done(null, user);
});
  
  passport.deserializeUser(function (id, done) {
    console.log("deserializeUser"); 
    console.log(id);

    user.get_Sp_userInfo(id).then(userInfo=>{
        done(null,userInfo)
    })
  });
  

  // passport local strategy for local-login, local refers to this app
  passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  }, function (user, password, done) {
    dbUserInfo.get_Sp_userInfo(user).then(userInfo=>{
          if(userInfo.status == 200 && userInfo.error == undefined){
            let recordset = userInfo.result.recordset;
            if(recordset.length > 0){
                let  passCompare = recordset[0].Password === password;
                if(passCompare){
                  return  done(null, {
                          username : recordset[0].UserName,
                          roleid   : recordset[0].RoleID,
                          role     : recordset[0].Role,
                          email    : recordset[0].Email
                      })
                }else{
                 return done(null, false, {message: 'Incorrect password'});
                }
            }else{
              return done(null, false, {message: `No user found  ${user}`});
            }
          }else{
            return  done(null, false, {message: `No user found  ${user}`});
          }
      }).catch(error=>{
           return  done(null, false, {message: `ERROR`});
      })
  }));