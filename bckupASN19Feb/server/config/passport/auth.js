const crypto = require('crypto');

module.exports = {
    isLoggedIn: function isLoggedIn(req, res, next) {
        // console.log("req.isAuthenticated()");  
    if (req.isAuthenticated()){
        // console.log(req.isAuthenticated());
        return next();
    }else{
        // console.log("user login false");
        return res.sendStatus(401);
    }       
 
    }
}