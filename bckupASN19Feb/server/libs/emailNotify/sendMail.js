var nodemailer = require('nodemailer');

exports.notifyToBuyer = function(senderInfo,reciverInfo,subject,mailBody){
    return new Promise((resolve,reject)=>{
        try {
            let transporterAccount = null;
            if(senderInfo != null){
                //DB channel
                transporterAccount ={
                    service :senderInfo.service,
                    user : senderInfo.EmailUserName,
                    pass : senderInfo.Password
                }                        
            }
            else{
                                 
            }              
    
            if(transporterAccount != null){
                var transporter = nodemailer.createTransport({
                    service: transporterAccount.service,
                    auth: {
                        user: transporterAccount.user,
                        pass: transporterAccount.pass
                    }
                });
            
                var mailOptions = {
                    from: transporterAccount.user,
                    to: _userMailId.toString(),
                    subject: subject ,
                    text : "" 
                };
           
                transporter.sendMail(mailOptions, function(error, info){                                           
                        if (err) {
                            return resolve({"status" : 401 , "result" : err });                                       
                        } else {                                  
                            if(typeof info !== 'undefined'){
                                return resolve({"status" : 200 , "result" : info.response }) ; 
                            }
                            else{
                                return resolve({"status" : 401 , "result" : "Someting wrong" });
                            }                                                
                        }
                }); 
            }
                        
               
    
        } catch (error) {
          return resolve(error);
      }
    })
}