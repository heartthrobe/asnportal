let  fs       = require('fs');
     path     = require('path');
     sql      = require('mssql');

     exports.get_Sp_Po_Detail = function(userName){
        return new Promise((resolve,reject)=>{
            try {
                let sqlConfig = JSON.parse(fs.readFileSync(path.join(__dirname,'..','config','mssql','sqlConfig.json'), 'utf8'));
                sql.connect(sqlConfig).then(pool => {
                    return pool.request()
                    .input('activated', 1)
                    .execute('GetPO')
                    
                }).then(result => {
                    sql.close();
                    resolve({
                        status:200,
                        msg : "PO information ",
                        error : undefined,
                        result : result.recordset
                    })
                    
                }).catch(err => {
                    // ... error checks
                    sql.close();
                    resolve({
                        status:400,
                        msg : "Error occured while fetching PO information",
                        error : err,
                        result : ''
                    })
                   
                })
    
            } catch (error) {
                reject({
                    status:400,
                    msg : "syntax error",
                    error : error,
                    result : ''
                })
            }
        })
    }

    