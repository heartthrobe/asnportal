//comman sp
let  fs       = require('fs');
     path     = require('path');
     sql      = require('mssql');

     exports.get_Sp_userInfo = function(userName){
        return new Promise((resolve,reject)=>{
            try {
                let sqlConfig = JSON.parse(fs.readFileSync(path.join(__dirname,'..','config','mssql','sqlConfig.json'), 'utf8'));
                sql.connect(sqlConfig).then(pool => {
                    return pool.request()
                    .input('userName',  userName )
                    .input('activated', 1)
                    .execute('GetUsers')
                    
                }).then(result => {
                    sql.close();
                    resolve({
                        status:200,
                        msg : "User information ",
                        error : undefined,
                        result : result
                    })
                    
                }).catch(err => {
                    // ... error checks
                    console.log(err);
                    sql.close();
                })
    
            } catch (error) {
                reject({
                    status:400,
                    msg : "syntax error",
                    error : error,
                    result : ''
                })
            }
        })
    }
    
    exports.insert_Sp_userInfo = function(userInfo){
        return new Promise((resolve,reject)=>{
            try {
                let sqlConfig = JSON.parse(fs.readFileSync(path.join(__dirname,'..','config','mssql','sqlConfig.json'), 'utf8'));
                sql.connect(sqlConfig).then(pool => {
                    return pool.request()
                    .input('userName',  userInfo.userName )
                    .input('roleID', userInfo.roleID)
                    .input('password',  userInfo.password )
                    .input('email', userInfo.email)
                    .input('MobileNo',  userInfo.MobileNo )
                    .input('locked',0)
                    .input('resetPassword',0)
                    .input('activated', 1)
                    .input('addUser',  userInfo.addUser )
                    .execute('InsertUser')
                    
                }).then(result => {
                    console.log(result);
                    sql.close();
                    resolve({
                        status:200,
                        msg : "User information ",
                        error : undefined,
                        result : result
                    })
                    
                }).catch(err => {
                    console.log(err);
                    // ... error checks
                    console.log(err);
                    sql.close();
                })
    
            } catch (error) {
                reject({
                    status:400,
                    msg : "syntax error",
                    error : error,
                    result : ''
                })
            }
        })
    }


//#region  Vendor Start

exports.inset_Sp_VendorInfo = function(vendorInfo){
    console.log("222222",vendorInfo)
    return new Promise((resolve,reject)=>{
        try {
            let sqlConfig = JSON.parse(fs.readFileSync(path.join(__dirname,'..','config','mssql','sqlConfig.json'), 'utf8'));
            sql.connect(sqlConfig).then(pool => {
                return pool.request()
                .input('vendorCode', vendorInfo.vendorCode )
                .input('vendorName', vendorInfo.vendorName)
                .input('emailID',vendorInfo.emailID)                
                .input('mobileNo', vendorInfo.mobileNo)
                .input('division', vendorInfo.division)
                .input('buyerName', vendorInfo.buyerName)
                .input('buyerEmailID', vendorInfo.buyerEmailID)
                .input('locked', 0)
                .input('activated', 1)
                .input('updateUser', 'SuperAdmin')
                .execute('InsertVendor')
                
            }).then(result => {
                sql.close();
                resolve({
                    status:200,
                    msg : "Record Inserted Successfully ",
                    error : undefined,
                    result : result
                })
                
            }).catch(err => {
                // ... error checks
                console.log(err);
                sql.close();
            })

        } catch (error) {
            reject({
                status:400,
                msg : "syntax error",
                error : error,
                result : ''
            })
        }
    })
}

exports.get_Sp_BuyerInfo = function(){
    console.log("222222")
    return new Promise((resolve,reject)=>{
        try {
            let sqlConfig = JSON.parse(fs.readFileSync(path.join(__dirname,'..','config','mssql','sqlConfig.json'), 'utf8'));
            sql.connect(sqlConfig).then(pool => {
                return pool.request()
                .input('roleId', 1)
                .input('activated',1)
                .execute('GetBuyerDetails')
                
            }).then(result => {
                sql.close();
                resolve({
                    status:200,
                    msg : "REceived All Buyer Record",
                    error : undefined,
                    result : result
                })
                
            }).catch(err => {
                // ... error checks
                console.log(err);
                sql.close();
            })

        } catch (error) {
            reject({
                status:400,
                msg : "syntax error",
                error : error,
                result : ''
            })
        }
    })
}

exports.get_Sp_VendorInfoIdWise = function(){
    console.log("222222")
    return new Promise((resolve,reject)=>{
        try {
            let sqlConfig = JSON.parse(fs.readFileSync(path.join(__dirname,'..','config','mssql','sqlConfig.json'), 'utf8'));
            sql.connect(sqlConfig).then(pool => {
                return pool.request()
                .input('roleId', 1)
                .input('activated',1)
                .execute('GetVendorDetailsIdWise')
                
            }).then(result => {
                sql.close();
                resolve({
                    status:200,
                    msg : "Received Vendor Record ID Wise",
                    error : undefined,
                    result : result
                })
                
            }).catch(err => {
                // ... error checks
                console.log(err);
                sql.close();
            })

        } catch (error) {
            reject({
                status:400,
                msg : "syntax error",
                error : error,
                result : ''
            })
        }
    })
}
//#endregion




