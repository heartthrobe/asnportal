var logger           = require('morgan');
    express          = require('express'),
    app              = express(),
    passport         = require('passport'),
    LocalStrategy    = require('passport-local').Strategy,
    bodyParser       = require('body-parser'),
    session          = require('express-session');
    cors             = require('cors');
    methodOverride   = require('method-override');
    cookieParser     = require('cookie-parser');
    bodyParser       = require('body-parser');
    expressValidator = require('express-validator');
    path             = require('path');
    fs               = require('fs');
    passportConfig        = require('./config/passport/passport'); 
    
    
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    app.set('view engine', 'ejs');
    app.use(logger('dev'));
    

    app.use(methodOverride());
    app.use(cookieParser());

    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ extended: true }));

    // body-parser for retrieving form data
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json()); 
    app.use(cors());

    app.set('view engine', 'handlebars');
    app.use(expressValidator());


    //Express-session
    app.use(session({
        secret: "ASNPortal123",
        cookie: { maxage : 6000000 } ,
        resave: false,
        saveUninitialized: false
    }))
    app.use(passport.initialize())
    app.use(passport.session())
    app.use(require('flash')());


/*----------------------------------------------------------------------------*/
    //#region server Routes file require(node)
    var userMaster = require('./routes/user_master/user_master_route');
    var vendorInfo = require('./routes/vender_profile/vender_profile_route')   
    var buyerInfo = require('./routes/buyer_Profile/buyer_profile_route')   
    var pendingOrderDetail = require('./routes/pending_po/pending_po_details_route');
        
    //#endregion
    //#region server Routes(node)
    app.use('/asnuser',userMaster);
    app.use('/asnVendor', vendorInfo);
    app.use('/asnBuyer', buyerInfo);
    app.use('/pendingOrder', pendingOrderDetail);

    
    //#endregion
/*----------------------------------------------------------------------------*/
    //#region Client Routes(angular)

    app.use(express.static(path.join(__dirname,'../client','dist/client')));
    app.use('/', express.static(path.join(__dirname,'../client','dist/client')));   
    app.use('/admin', express.static(path.join(__dirname,'../client','dist/client')));
    app.use('/admin/registerVendor', express.static(path.join(__dirname,'../client','dist/client')));
    app.use('/admin/userMaster', express.static(path.join(__dirname,'../client','dist/client')));
    app.use('/admin/createVendor', express.static(path.join(__dirname,'../client','dist/client')));
    app.use('/admin/pendongPoList', express.static(path.join(__dirname,'../client','dist/client')));
    

    //#endregion
/*----------------------------------------------------------------------------*/

// catch 404 and forward to error handler
    app.use(function(req, res, next) {
        next(createError(404));
    });
  
// error handler
  
    app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    console.log('TestUrl',req.url)
    // res.writeHead(302, {
    //   'Location': 'localhost:4009/'+req.url
    //   //add other headers here...
    // });
    // render the error page
    res.status(err.status || 500);
    res.send(err.status);
    });
    
    module.exports = app;