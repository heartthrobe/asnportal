import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private config = { hour: 7, minute: 15, meriden: 'PM', format: 12 };
  constructor(
    private fb: FormBuilder,
    // private authService: AuthService,
    private router: Router,
    // private api: ApiService,
    private formBuilder: FormBuilder,
    
  ) { }
  title = 'client';
}
