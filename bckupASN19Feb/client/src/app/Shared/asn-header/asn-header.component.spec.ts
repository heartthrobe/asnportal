import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsnHeaderComponent } from './asn-header.component';

describe('AsnHeaderComponent', () => {
  let component: AsnHeaderComponent;
  let fixture: ComponentFixture<AsnHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsnHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsnHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
