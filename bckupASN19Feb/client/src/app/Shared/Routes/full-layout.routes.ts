import { Routes } from "@angular/router";


export const Full_ROUTES:Routes=[
    {
        path:'',
        loadChildren:'./LoginModule/login.module#LoginModule'
    },
    {
        path:'admin',
        loadChildren:'./Admin/admin.module#AdminModule',        
    },

    
]