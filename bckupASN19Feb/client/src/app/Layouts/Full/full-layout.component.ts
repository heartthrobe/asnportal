import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'app-full-layout',
    templateUrl: './full-layout.component.html',
    styleUrls: ['./full-layout.component.css']
  })
  export class FullLayoutComponent {
    title = 'client';
    constructor(private router: Router, private location: Location, private route: ActivatedRoute) { }
  }