import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { LoginRoutingModule } from './login-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AsnLoginComponent } from './asn-login/asn-login.component';

  //Components
  @NgModule({
    declarations: [      
     AsnLoginComponent
    ],
    imports:[
        CommonModule,
        LoginRoutingModule,        
        FormsModule ,
        ReactiveFormsModule,
        HttpClientModule
      ],
        
    providers: [  ],
     bootstrap: []
    })
    export class LoginModule { }