import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsnLoginComponent } from './asn-login.component';

describe('AsnLoginComponent', () => {
  let component: AsnLoginComponent;
  let fixture: ComponentFixture<AsnLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsnLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsnLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
