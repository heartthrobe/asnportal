import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import  {LoginService}  from  '../login.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-asn-login',
  templateUrl: './asn-login.component.html',
  styleUrls: ['./asn-login.component.css']
})
export class AsnLoginComponent implements OnInit {
  asnLogin : FormGroup;

  constructor(private asnLoginApi:LoginService,private router: Router) { }

  ngOnInit() {
    this.asnLogin = new FormGroup({
        username : new FormControl(''),
        password : new FormControl(''),
      });
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    let user = this.asnLogin.value
    let loginUser = {
      username : user.username,
      password : user.password
    }
    this.asnLoginApi.getAsnUser(loginUser).subscribe(res => {
          console.log(res);
          if(res.status == 200){
              if(res.result.roleid == "1"){
                 this.router.navigate(['/admin']);
              }else if(res.result.roleid == "2"){

              }else if(res.result.roleid == "3"){
                
              }else if(res.result.roleid == "4"){
                
              }
          }else{

          }
    },(err)=>{

    })
  }

}
