import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsnLoginComponent } from './asn-login/asn-login.component';


const routes: Routes = [
    
    {
        path: '',
        component: AsnLoginComponent,  
        pathMatch:'full'     
    }
    // {
    //     path: 'forgotPassword',
    //     component: ResetPasswordComponent,  
    //     pathMatch:'full'     
    // },
    // {
    //     path: 'recoverPassword/:id',
    //     component: ConfirmPasswordComponent,  
    //     pathMatch:'full'     
    // },
    // {
    //     path: 'change_password/:id/:status',
    //     component: ConfirmPasswordComponent,  
    //     pathMatch:'full'     
    // }
   
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }