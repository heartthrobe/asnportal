import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { FullLayoutComponent } from './Layouts/Full/full-layout.component';
import { Full_ROUTES } from './Shared/Routes/full-layout.routes';
import { PartialLayoutComponent } from './Layouts/Partial/partial-layout.component';
import { Partial_ROUTES } from './Shared/Routes/partical-layout.routes';

const appRoutes: Routes = [

  { path: '', component: PartialLayoutComponent, children: Partial_ROUTES },
  { path: '', component: FullLayoutComponent, children: Full_ROUTES },

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
