import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AdminRoutingModule } from './admin-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendorRegistrationComponent } from './vendor-registration/vendor-registration.component';
import { AsnUserMasterComponent } from './asn-user-master/asn-user-master.component';

import { CreateAnsComponent } from './create-ans/create-ans.component';
import {  
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatSelectModule,
  MatButtonModule,
  MatCardModule, MatTooltipModule,
  MatListModule, MatToolbarModule,
  MatFormFieldModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSlideToggle,
  MatSlideToggleModule, MatRadioModule,
  MatTabsModule, MatCheckboxModule, MatProgressBarModule, MatExpansionModule, 
  MatStepperModule, MatDialogModule, MatAutocompleteModule,MatMenuModule} from '@angular/material';
import { PendingPoListComponent } from './POMaster/pending-po-list/pending-po-list.component';

 //Components
 @NgModule({
    declarations: [      
      VendorRegistrationComponent, AsnUserMasterComponent,  CreateAnsComponent, PendingPoListComponent
    ],
    imports:[
        CommonModule,       
        AdminRoutingModule,        
        FormsModule ,
        ReactiveFormsModule,
        HttpClientModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatButtonModule,
        MatMenuModule,
        MatCardModule,
        MatDialogModule,
        MatToolbarModule,
        MatStepperModule,
        MatFormFieldModule,
        MatSidenavModule,
        MatSlideToggleModule,
        MatListModule, MatTooltipModule,
        MatSnackBarModule,
        MatRadioModule, MatTabsModule, MatCheckboxModule,MatProgressBarModule, MatExpansionModule,

      ],
        
    providers: [],
     bootstrap: []
    })
    export class AdminModule { }