import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const _asnUserUrl = "/asnuser";
const _asnVendorUrl = "/asnVendor";
const _asnBuyerUrl="/asnBuyer";
const _poMasterUrl = "/pendingOrder";

@Injectable({
    providedIn: 'root'
  })

  export class AdminService {
    constructor(private http: HttpClient) { }

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      // return an observable with a user-facing error message
      return throwError(error);
    };
  
    private extractData(res: Response) {
      let body = res;
      return body || { };
    }

    //#region LogIN Module API       
          getAsnUserRegister(data): Observable<any> {         
              return this.http.post(_asnUserUrl + '/register',data,httpOptions).pipe(
                map(this.extractData),
                catchError(this.handleError));
          }
    //#endregion

      //#region Pending Order Module API       
          getPoDetail(): Observable<any> { debugger        
            return this.http.get(_poMasterUrl + '/podetail',httpOptions).pipe(
              map(this.extractData),
              catchError(this.handleError));
          }
     //#endregion
    //#region Vendor Module API       
        addAsnVendorRegister(data): Observable<any> {         
          return this.http.post(_asnVendorUrl + '/registerVendor',data,httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
        }
        getBuyerDetails(): Observable<any> {         
          return this.http.get(_asnBuyerUrl + '/getBuyerDetails',httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
        }
        getVendorDetails(): Observable<any> {         
          return this.http.get(_asnVendorUrl + '/getVendorDetailsIdWise',httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
        }
    //#endregion
  }