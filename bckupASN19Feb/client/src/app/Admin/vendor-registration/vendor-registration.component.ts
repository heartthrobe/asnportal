import { Component, OnInit, ViewChild, OnChanges, SimpleChanges} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {noWhitespaceValidator} from '../../AsnCommanStuff/AsnCustomeValidator/asnValidators'
import {AdminService} from '../admin.service'

@Component({
  selector: 'app-vendor-registration',
  templateUrl: './vendor-registration.component.html',
  styleUrls: ['./vendor-registration.component.css']
})
export class VendorRegistrationComponent implements OnInit {

  @ViewChild('form') form;
  VendorRegistration: FormGroup;
  successWarning:boolean=false;
  alertWarning:boolean=false;
  buyerList=[];
  AAccess = [
    { id: 1, name: 'Use Qlik Security' },
    { id: 2, name: 'Manage In Ping' }
  ];
  constructor(private adminService:AdminService, private router: Router, private formBuilder: FormBuilder,) {

  }
  ngOnInit() {
    this.adminService.getBuyerDetails().subscribe(buyerInfo=>{
      console.log("111",buyerInfo)
      console.log("111",buyerInfo.result.result.recordsets)
      this.buyerList=buyerInfo.result.result.recordsets;
      this.VendorRegistration.get("vendorBuyer").setValue(this.buyerList)
      
    })
    this.VendorRegistration = this.formBuilder.group({
      // id: '',
      vendorCode:['1',Validators.required],
      vendorName: [null,[Validators.required,noWhitespaceValidator]],
      vendorEmail: [null,Validators.required,noWhitespaceValidator],
      vendorBuyer : new FormControl(null, Validators.required),     
      vendorMobileNo :[null,[Validators.required,noWhitespaceValidator]],
      vendorDivision:[null,[Validators.required,noWhitespaceValidator]],
      vendorBuyerEmail : new FormControl(this.AAccess, Validators.required),
        
    })
  }
  onFormSubmit(form:NgForm) {
    console.log(form)
    this.adminService.addAsnVendorRegister(form).subscribe(res=>{
      if(res.result.returnValue>=0)
      {        this.successWarning=true;
      console.log("Vendor Registration Successfull");
      }
      else{
        this.successWarning=true;
      console.log("Something went wrong");
      }
    })
  }

}
