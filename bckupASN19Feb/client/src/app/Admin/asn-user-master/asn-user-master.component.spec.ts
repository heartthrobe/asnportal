import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsnUserMasterComponent } from './asn-user-master.component';

describe('AsnUserMasterComponent', () => {
  let component: AsnUserMasterComponent;
  let fixture: ComponentFixture<AsnUserMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsnUserMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsnUserMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
