
import { Component, OnInit, ViewChild, OnChanges, SimpleChanges} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {noWhitespaceValidator} from '../../AsnCommanStuff/AsnCustomeValidator/asnValidators'
import  {AdminService}  from  '../admin.service'

@Component({
  selector: 'app-asn-user-master',
  templateUrl: './asn-user-master.component.html',
  styleUrls: ['./asn-user-master.component.css']
})
export class AsnUserMasterComponent implements OnInit {
  @ViewChild('form') form;
  UserRegistration: FormGroup;
  UserRoles = [
    { id: 2, name: 'Admin' },
    { id: 4, name: 'Buyer' },
    { id: 3, name: 'Vendor'}
  ];
  constructor(private asnUserApi:AdminService,private router: Router, private formBuilder: FormBuilder) {

  }
  ngOnInit() {
    this.UserRegistration = this.formBuilder.group({
      // id: '',
      userCode:['1',Validators.required],
      userName :[null,Validators.required],
      userRole:new FormControl({}, Validators.required),
      emailId :[null,Validators.required],
      password :[null,Validators.required],
      mobileNo : [null,Validators.required]
    })
  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    let user = this.UserRegistration.value;
    let userDetail = {
      userCode : user.userCode.trim(),
      userName : user.userName.trim(),
      userRole : user.userRole.trim(),
      emailId  : user.emailId.trim(),
      password : user.password.trim(),
      mobileNo : user.mobileNo
    };
    console.log(userDetail);
    this.asnUserApi.getAsnUserRegister(userDetail).subscribe(res=>{
        console.log(res);
    },err =>{
       console.log(err);
    }) 

  }
}
