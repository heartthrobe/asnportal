
import { Component, OnInit, ViewChild, OnChanges, SimpleChanges} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {noWhitespaceValidator} from '../../AsnCommanStuff/AsnCustomeValidator/asnValidators'

@Component({
  selector: 'app-create-ans',
  templateUrl: './create-ans.component.html',
  styleUrls: ['./create-ans.component.css']
})
export class CreateAnsComponent implements OnInit {

  @ViewChild('form') form;
  frmAsnCreation: FormGroup;
  constructor(private router: Router, private formBuilder: FormBuilder) {}
  buyerList = [
    { id: 1, name: 'Manoj' },
    { id: 2, name: 'Onkar' },
    { id: 2, name: 'Sheshadri' }
  ];
  ngOnInit() {
    this.frmAsnCreation = this.formBuilder.group({
      // id: '',
      ASNNo:['1',Validators.required],
      ASNDt: [null,[Validators.required,noWhitespaceValidator]],
      shippingNo: [null,Validators.required,noWhitespaceValidator],
      invoiceNo : [null,Validators.required,noWhitespaceValidator],
      invoiceDate :[null,[Validators.required,noWhitespaceValidator]],
      division:[null,[Validators.required,noWhitespaceValidator]],
      buyer : new FormControl(this.buyerList, Validators.required),
      OurReceiptNo:[null,[Validators.required,noWhitespaceValidator]],
      deliveryChallanNo:[null,[Validators.required,noWhitespaceValidator]],
      transporterName:[null,[Validators.required,noWhitespaceValidator]],
      isActive:true, 
      insertedBy:[null], 
      insertedDate:[''],
      updatedBy : '', 
      updatedDate : '', 
      LoginUserID:'',
      UserAllocate : ''   
    })
  }
  onFormSubmit(form:NgForm) {
    console.log(form)
  }

}
