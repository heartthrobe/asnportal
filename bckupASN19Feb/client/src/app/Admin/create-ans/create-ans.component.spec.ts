import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAnsComponent } from './create-ans.component';

describe('CreateAnsComponent', () => {
  let component: CreateAnsComponent;
  let fixture: ComponentFixture<CreateAnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
