import { Component, OnInit , ViewChild, Injectable,Inject} from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { AdminService } from '../../admin.service';
import { Router, ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import {  FormGroup, FormControl, NgForm,FormBuilder,Validators } from '@angular/forms';
// import { identifier } from '../../../../node_modules/@types/babel-types';
import { MatSnackBar, MatSnackBarHorizontalPosition,MatSnackBarConfig,
  MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';
  import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AppService } from '../../../app.service';
import { isObject } from 'ngx-bootstrap/chronos/utils/type-checks';
import { resolve } from 'q';
// import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-pending-po-list',
  templateUrl: './pending-po-list.component.html',
  styleUrls: ['./pending-po-list.component.css']
})
export class PendingPoListComponent implements OnInit {
  displayedColumns: string[] = ['PO No', 'Sch No', 'Item Desc', 'Rate','Line Qty', 'Sch Qty', 'Sch Bal Qty', 'ItemAmt','DiscAmt','Amount'];
  poDetails = [];
  poSelectedList = [];
  fgPO : FormGroup ;

  SelectedPendingPoList:boolean=false;
  ShowPendingPoList:boolean=true;

  constructor(private pendingOrderApi:AdminService,private router: Router,private fb:FormBuilder,public dialog: MatDialog) {
   
   }

  ngOnInit() {
    
 
    this.pendingOrderApi.getPoDetail().subscribe(pendingorderResponse=>{
      console.log(pendingorderResponse);
        if(pendingorderResponse.status == 200){
           
           let dbDetail : [] = pendingorderResponse.result;
           this.poDetails = dbDetail;
           console.log(this.poDetails)
        }
    },error=>{
        console.log(error);
    });

           
  }

  onCheckChange(event,po){
    console.log(event);
      if(event.target.checked){
        this.poSelectedList.push(po);
        console.log(this.poSelectedList);
      }else{
          if(this.poSelectedList.length > 0){
           this.poSelectedList =this.poSelectedList.filter((v,i,a)=>{
                  if(v.PendingPoDetailsId != po.PendingPoDetailsId){
                      return v;
                  }
              })
          }
      }
  }

  onSubmitSelectedPoList(){
      if(this.poSelectedList.length> 0){
          this.SelectedPendingPoList = true;
          this.ShowPendingPoList = false;

      }else{
          console.log("Please select at list one item")
      }

  }

  onSubmitPOList(): void {
    const dialogRef = this.dialog.open(confirmPoOrder, {
      height: '560px',
            width: '800px',
      data: { name: "asfsaf", animal: "safdasd" }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

}

@Component({
  selector: 'confirm-Po-List',
  templateUrl: 'confirm-Po-List.html',
})
export class confirmPoOrder{
  confirmPoList: FormGroup;
  isPopupOpened = true;
  form: NgForm;
  // timezones: TimeZone[] = [];
  
  buyerList = [
    { id: 1, name: 'Manoj' },
    { id: 2, name: 'Onkar' },
    { id: 2, name: 'Sheshadri' }
  ];


  constructor(private dialog: MatDialog, private commanAPI: AppService, public dialogRef: MatDialogRef<confirmPoOrder>,
    @Inject(MAT_DIALOG_DATA)  public api: AdminService,  public snackbar: MatSnackBar,private formBuilder: FormBuilder,) {
      this.confirmPoList = this.formBuilder.group({
        // id: '',
        ASNNo:['1',Validators.required],
        ASNDt: [null,[Validators.required]],
        shippingNo: [null,Validators.required],
        invoiceNo : [null,Validators.required],
        invoiceDate :[null,[Validators.required]],
        division:[null,[Validators.required]],
        buyer : new FormControl(this.buyerList, Validators.required),
        emailId:[null,[Validators.required]],
        OurReceiptNo:[null,[Validators.required]],
        deliveryChallanNo:[null,[Validators.required]],
        transporterName:[null,[Validators.required]],
        lrNo:[null,[Validators.required]],  
      })
    var isEmpty = function (obj) {
      return Object.keys(obj).length === 0;
      
    }
   
   

    // this.TimezoneServer = this.formBuilder.group({
    //   // id: '',

    //   timeZoneLocation : new FormControl(this.timezones, Validators.required),

    //   prefferdChanel: new FormControl(this.places,Validators.required), 
    //   // timeZoneLocation: new FormControl(this.timezonename, Validators.required),

    //   // prefferdChanel: new FormControl(this.prefferdChanel, Validators.required),

    // })

  }


  
}
 
