import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingPoListComponent } from './pending-po-list.component';

describe('PendingPoListComponent', () => {
  let component: PendingPoListComponent;
  let fixture: ComponentFixture<PendingPoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingPoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingPoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
