import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorRegistrationComponent } from './vendor-registration/vendor-registration.component';
import { AsnUserMasterComponent } from './asn-user-master/asn-user-master.component';
import { CreateAnsComponent } from './create-ans/create-ans.component';
import { PendingPoListComponent } from './POMaster/pending-po-list/pending-po-list.component';

const routes: Routes = [  
    {
        path: 'registerVendor',
        component: VendorRegistrationComponent,  
        pathMatch:'full'     
    },
    {    
      path: 'userMaster',
      component: AsnUserMasterComponent,  
      pathMatch:'full'     
  },
  {    
      path: 'createAsn',
      component: CreateAnsComponent,  
      pathMatch:'full'     
  },
  {    
      path: 'createVendor',
      component: VendorRegistrationComponent,  
      pathMatch:'full'     
  },
  {    
      path: 'pendongPoList',
      component: PendingPoListComponent,  
      pathMatch:'full'     
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }