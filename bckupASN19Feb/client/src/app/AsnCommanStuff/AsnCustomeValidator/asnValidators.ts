import { AbstractControl } from "@angular/forms";

export function PingConfirmPassword(control:AbstractControl){
    let consirmPass = control.value;

        // control value (e.g. password)
        let password = control.root.get("certPassword");

        // value not equal
        if (control && (control.value !==null || control.value !==undefined))  {
         const cnfPass=control.value;
         const password=control.root.get("certPassword");
         if(password){
             const passvalue=password.value;
             if(passvalue!==cnfPass)
             {
                 return {
                     isError:true
                 };
             }
         }
        }
        return null;

}

export function phoneNumberValidator(control: AbstractControl) { 
    
    
    // const valid = /^\d+$/.test(control.value);
    // return valid ? null : { invalidNumber: { valid: false, value: control.value } };
    if (control && (control.value !==null || control.value !==undefined))  {
        const inputNumber=control.value;
        const NUMBER_REGEXP = /^[0-9]/;
        if (NUMBER_REGEXP.test(inputNumber.value)) {
            return null;
         }
        return {
         invalidNumber: true
         };
    }
     
  }

  export function noWhitespaceValidator(control:AbstractControl){
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
}